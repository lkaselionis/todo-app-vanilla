var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        color: 'Blue',
        groupClass: 'list-group',
        groupItem: 'list-group-item',
        seen: false,
        sampleColors: ['Blue', 'Yellow', 'Black', 'Red'],
        userInput: ''
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
});