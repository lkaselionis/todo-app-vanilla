let todoList = [],
    todoInputElement, todoListElement;

docReady(function () {
    clearAndFocusInput();

    todoInputElement = document.getElementById('todo-input');
    todoListElement = document.getElementById('todo-list');

    todoInputElement.addEventListener('submit', submitHandler);
});

    function docReady(fn) {
        // see if DOM is already available
        if (document.readyState === "complete" || document.readyState === "interactive") {
            // call on next available tick
            setTimeout(fn, 1);
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

function makeId(length) {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function clearAndFocusInput() {
    const input = document.querySelector('.todo-input-field');
    input.value = '';
    input.focus();
}

function submitHandler(e) {
    e.preventDefault();
    let newTodo = this.elements['todo'].value;
    newTodo = newTodo.trim();
    clearAndFocusInput();
    if (newTodo) {
        addTodoItem(newTodo);
    }
}

function completeHandler(e) {
    let sourceId = this.getAttribute('id');
    let id = sourceId.replace('check_', '');

    todoList.filter(function (item) {
        if (item.id === id) {
            item.isCompleted = !item.isCompleted;
        }
    })

    updateView();
}

function deleteHandler(e) {
    let sourceId = this.getAttribute('id');
    let id = sourceId.replace('delete_', '');

    let [item] = todoList.filter(function (item) {
        return (item.id === id)
    });

    let index = todoList.indexOf(item);
    if (index !== -1) todoList.splice(index, 1);

    updateView();
}

function updateView() {
    console.log(todoList);
    // deleting previously rendered items
    let range = document.createRange();
    range.selectNodeContents(todoListElement);
    range.deleteContents();

    todoList.forEach(function (item) {
        renderTodoItem(item);
    })
}

function addTodoItem(value) {
    todoList.push({
        'id': makeId(6),
        'title': value,
        'isCompleted': false
    });
    updateView();
}

function renderTodoItem(itemObj) {
    let item = document.createElement('li');
    item.classList.add('todo-item', 'todo-item-control-wrapper', 'list-group-item');

    let checkbox = '<div class="custom-control custom-checkbox">';
    checkbox += '<input type="checkbox" class="todo-item-checkbox custom-control-input" id="check_' + itemObj.id + '"></input>';
    checkbox += '<label class="todo-item-title custom-control-label" for="check_' + itemObj.id + '">' + itemObj.title + '</label>';
    checkbox += '</div>';

    let button = '<button type="button" class="todo-item-delete btn btn-light btn-sm" id="delete_' + itemObj.id + '">Delete</button>';
    item.innerHTML = checkbox + button;
    todoListElement.appendChild(item);

    let itemCheckbox = item.querySelector('.todo-item-checkbox');

    itemCheckbox.addEventListener('change', completeHandler);
    let itemButton = item.querySelector('.todo-item-delete');
    itemButton.addEventListener('click', deleteHandler);

    itemCheckbox.checked = itemObj.isCompleted;
    if (itemObj.isCompleted) {
        item.classList.add('completed');
    }
}